<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// weekly score page
Route::get('score/week/{week}', ['as' => 'scores::view', 'uses' => 'GameController@scores']);

/**
 * standing tables page
 * type: string - optional: eg, week|date
 * time: integer - optional: eg,   1|10122018
 **/
Route::get('standing/{type?}/{time?}', ['as' => 'standing::table', 'uses' => 'GameController@standing']);
