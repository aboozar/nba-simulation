<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// get all scores for a week games
Route::get('games/{week}', [
    'as'   => 'api:week:games',
    'uses' => 'Api\\GameController@games',
]);

// get all season players list
Route::get('players', [
    'as'   => 'api::players',
    'uses' => 'Api\\GameController@players',
]);

// get all standing data for week/total
Route::get('standing/{type?}/{time?}', [
    'as'   => 'api::standing',
    'uses' => 'Api\\GameController@standing',
]);

// starts the week games simulator by getting week number
Route::get('simulate/{week}', [
    'as'   => 'api::simulate',
    'uses' => 'Api\\GameController@runSimulator',
]);