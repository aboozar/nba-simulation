<?php

namespace App\Observers;

use App\Models\Game;
use App\Models\Standing;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/**
 * Class GameObserver
 * @package App\Observers
 */
class GameObserver
{
    /**
     * Handle the Game "updated" event.
     *
     * @param \App\Models\Game $Game
     *
     * @return void
     */
    public function updating (Game $game)
    {
        Log::info('game is updating...');


        /**
         * process game and mark it as processed if game state changed to `finished1
         * in simulator
         */
        if ($game->finished == 1 && $game->getOriginal('processed') == 0)
        {
            Log::info('game finish hook is running...');

            $homeScore    = $game->getHomeScore();
            $visitorScore = $game->getVisitorScore();

            Log::info("home score [$homeScore] vs visitor score [$visitorScore]");

            /**
             * basketball should have a winner so
             * we choose a random winner it results was same
             */
            if ($homeScore == $visitorScore)
            {
                // random winner
                if (mt_rand(0, 1)) {
                    $homeScore         = $homeScore + 3;
                    $game->quarter_h_4 = DB::raw("quarter_h_4 + 3");
                } else {
                    $visitorScore      = $visitorScore + 3;
                    $game->quarter_v_4 = DB::raw("quarter_v_4 + 3");
                }
            }

            if ($homeScore > $visitorScore) {
                /**
                 * home won
                 **/

                $winnerTeamId = $game->home_id;

                // updater winner
                Standing::where('team_id', $game->home_id)
                    ->where('game_id', $game->id)
                    ->update([
                        'win'          => DB::raw("win + 1"),
                        'score_earned' => DB::raw("score_earned + {$homeScore}"),
                        'score_lost'   => DB::raw("score_lost + {$visitorScore}"),
                    ]);

                //update looser
                Standing::where('team_id', $game->visitor_id)
                    ->where('game_id', $game->id)
                    ->update([
                        'loose'        => DB::raw("loose + 1"),
                        'score_earned' => DB::raw("score_earned + {$visitorScore}"),
                        'score_lost'   => DB::raw("score_lost + {$homeScore}"),
                    ]);

            } else {

                /**
                 * visitor won
                 **/

                $winnerTeamId = $game->visitor_id;

                // updater winner
                Standing::where('team_id', $game->visitor_id)
                    ->where('game_id', $game->id)
                    ->update([
                        'win'          => DB::raw("win + 1"),
                        'score_earned' => DB::raw("score_earned + {$visitorScore}"),
                        'score_lost'   => DB::raw("score_lost + {$homeScore}"),
                    ]);

                //update looser
                Standing::where('team_id', $game->home_id)
                    ->where('game_id', $game->id)
                    ->update([
                        'loose'        => DB::raw("loose + 1"),
                        'score_earned' => DB::raw("score_earned + {$homeScore}"),
                        'score_lost'   => DB::raw("score_lost + {$visitorScore}"),
                    ]);
            }

            $game->winner_id = $winnerTeamId;
            $game->processed = true;
        }
    }
}
