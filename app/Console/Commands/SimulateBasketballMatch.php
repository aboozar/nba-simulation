<?php
/**
 * Created by PhpStorm.
 * User: aboozar
 * Date: 11/22/18
 * Time: 7:30 PM
 */

namespace App\Console\Commands;

use App\Jobs\ProcessGameEvent;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Models\Game\Engine;


/**
 * This command starts a game simulation and finish it exactly in 240 s
 *
 * Simulator uses Game\Engine class for handling game-play
 *
 * Class SimulateBasketballMatch
 * @package App\Console\Commands
 */
class SimulateBasketballMatch extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'game:simulate {gameId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'starts a basketball game simulation by given game id';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct ()
    {
        parent::__construct();
    }

    /**
     * Execute the simulator
     *
     * @return mixed
     */
    public function handle ()
    {
        $gameId = $this->argument('gameId');

        $startTime = time();

        // initiate progress bar
        $bar = $this->output->createProgressBar(240); //sec
        $bar->start();


        Log::info("#SIMULATION for game {$gameId} starts at {$startTime}");

        // fetch game related info including teams data
        $engine = new Engine($gameId);

        // bypass started games
        if ($engine->getGame()->started)
        {
            Log::warning("#SIMULATION for game {$gameId} has started before! --- bypassed!");
            return;
        }

        // mark game as started
        $engine->markGameStarted();

        Log::info('fetching players info for each team');
        // get players ids for home team
        $homePlayers = $engine->getHomeTeamPlayers();
        // get players ids for visitor team
        $visitorPlayers = $engine->getVisitorTeamPlayers();

        // put all players into an array for random pickup
        $teamPlayers = [
            $engine->getGame()->home->id    => $homePlayers,
            $engine->getGame()->visitor->id => $visitorPlayers,
        ];

        // pickup random team as first attacker
        $currentAttacker = $engine->getRandomAttackerTeam();

        // passed time in second
        $passedTime = 0;

        // attack counter
        $attackCounter = 0;


        do {
            Log::info(sprintf('#SIMULATIONING attack [%s] by team [%s]', ++$attackCounter, $currentAttacker));

            // pick random final action for an attack
            $action = $engine->pickRandomAction();

            // calculate the score for final action
            $score = $engine->calculateScore($action);

            // fetch 5 random player as players that participated in one attack from current attacker team
            $currentAttackerPlayers = $teamPlayers[$currentAttacker];
            shuffle($currentAttackerPlayers);
            $pickedPlayers = mt_rand(1, 5);
            array_splice($currentAttackerPlayers, $pickedPlayers);

            // pick a random time between 3 to 24 sec as an attack time in seconds - minimum is just for reality
            $attackDuration = mt_rand(3, 24) / 12; // each minute is 5 (60/12) second in simulation (as task requested"


            // add elapsed time with last attack duration
            $passedTime += $attackDuration;

            // sleep for simulation time spending for an attack
            usleep($attackDuration * 1000000);

            // calculates current quarter
            $quarter = min(4, intval($passedTime / 60) + 1);


            Log::info(sprintf('quarter %s players [%s] action: %s', $quarter,
                implode(',', $currentAttackerPlayers),
                $action
            ));

            $eventDetail = [
                'quarter'      => $quarter,
                'game_id'      => $engine->getGame()->id,
                'team_id'      => $currentAttacker,
                'home_team_id' => $engine->getGame()->home->id,
                'score'        => intval($score),
                'week'         => $engine->getGame()->week,
            ];

            if ($score > 0)
            {
                $field = strtolower($action);

                // initiate player data
                $eventDetail ['player'] = [
                    'action'    => $field,
                    'player_id' => $currentAttackerPlayers[$pickedPlayers - 1],
                    "{$field}"  => 1,
                ];

                // initiate assist player data
                if ($score > 1 && count($currentAttackerPlayers) > 1) {
                    $eventDetail['assist'] = [
                        'player_id' => $currentAttackerPlayers[$pickedPlayers - 2],
                        "{$field}"  => 1,
                    ];
                }
            }

            /**
             * put action details to queue for simultaneously handling
             */
            ProcessGameEvent::dispatch($eventDetail);

            // changed attacker based on the latest attack action
            $currentAttacker = $engine->calculateAttacker($currentAttacker, $action);
            $bar->advance();
        } while (round($passedTime, 0) < 240);


        // mark match as finished
        Log::info(sprintf('mark match %d as finished', $gameId));

        // mark game as finished
        $engine->getGame()->finished = true;
        $engine->getGame()->save();


        $bar->finish();

        Log::info(sprintf('game %s #SIMULATION finished at %s', $gameId, (time() - $startTime)));
    }
}
