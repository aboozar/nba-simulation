<?php

namespace App\Console\Commands;

use App\Events\GameUpdated;
use App\Events\PlayerUpdated;
use App\Models\Game;
use App\Models\PlayerScore;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class RunPusherJob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'push:week {week}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Pushes changes to broadcast server';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct ()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle ()
    {
        $week = intval($this->argument('week'));

        $time = 0;
        do {
            try {
                Log::info(sprintf('#PUSH pushing game data to broadcaster for week %s sec %s', $week, $time));

                $data  = [];
                $games = Game::select([
                    'id',
                    'quarter_h_1 AS qh1', // abbreviation just for increasing the payload size
                    'quarter_h_2 AS qh2',
                    'quarter_h_3 AS qh3',
                    'quarter_h_4 AS qh4',
                    'quarter_v_1 AS qv1',
                    'quarter_v_2 AS qv2',
                    'quarter_v_3 AS qv3',
                    'quarter_v_4 AS qv4',
                    'winner_id AS w',
                    'home_attacks AS ha',
                    'visitor_attacks AS va',
                    'started AS s'
                    ])
                    ->where('week', $week)
                    ->where('started', true)
                    ->orderBy('id')
                    ->get();



                $games = $games->toArray();

                event(new GameUpdated($games)); // broadcast game updates

                $playerScore = PlayerScore::select([
                    'game_id',
                    'player_id',
                    'score1',
                    'score2',
                    'score3',
                    'assist',
                    'score',
                    'home_player'
                ])
                ->where('week', $week)->get();

                foreach ($playerScore as $score) {

                    $data [$score->game_id]['gid'] = $score->game_id;

                    if ($score->home_player) {

                        $data [$score->game_id]['hp'][] = $score->toArray();
                    } else {
                        $data [$score->game_id]['vp'][] = $score->toArray();
                    }
                }

                event(new PlayerUpdated($data)); // broadcast player updates


                Log::info('#PUSH latest updates pushed to broadcaster');

            } catch (\Exception $e) {
                Log::error($e->getMessage());
                continue;
            }

            sleep(5); // each 5 seconds -- hardcoded just for test
            $time = $time + 5;

        } while ($time < 300);
    }
}
