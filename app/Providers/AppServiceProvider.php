<?php

namespace App\Providers;

use App\Models\Game;
use App\Models\GameEvent;
use App\Observers\GameObserver;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // register model observers in order to handling events
        /**
         * warning fix for laravel queues migration with some mysql version
         */
        Schema::defaultStringLength(191);

        /**
         * game model observer
         */
        Game::observe(GameObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
