<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Standing
 * @package App\Models
 */
class Standing extends Model
{
    public $timestamps = false;

    protected $guarded = ['id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function team()
    {
        return $this->belongsTo('App\Models\Team', 'team_id', 'id');
    }
}
