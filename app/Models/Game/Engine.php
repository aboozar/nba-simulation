<?php
/**
 * Created by PhpStorm.
 * User: aboozar
 * Date: 11/22/18
 * Time: 7:41 PM
 */

namespace App\Models\Game;

use App\Enums\FinalActions;
use App\Models\Game;
use App\Models\Player;
use App\Models\Standing;


/**
 * Class Engine
 *
 * This class handle game related calculations like wht action happened,
 * what is the score for this action and etc.
 *
 * @package App\Models\Game
 */
class Engine
{
    /**
     * @var Game
     */
    protected $game;

    /**
     * ID of teams that are going to play in this game
     * @var array
     */
    protected $teams;


    public function __construct (int $gameId)
    {
        $this->game = Game::where('id', $gameId)
            ->first();

        $this->teams = [
            $this->game->home_id,
            $this->game->visitor_id,
        ];
    }

    /**
     * @return Game
     */
    public function getGame (): Game
    {
        return $this->game;
    }

    /**
     * returns array of teams for the game
     *
     * @return array
     */
    public function getTeams (): array
    {
        return $this->teams;
    }

    /**
     * calculate attacker team after current action/attack
     *
     * @param string $currentAttacker
     * @param string $finalAction
     *
     * @return string
     */
    public function calculateAttacker (string $currentAttacker, string $finalAction): string
    {
        $teams = $this->getTeams();

        // detect current attacker team
        $currentAttackerIndex = (($teams[0] == $currentAttacker) ? 0 : 1);

        switch ($finalAction) {
            // after successful shoot and getting score the attacker team should be change
            case FinalActions::SCORE2:
            case FinalActions::SCORE3:
                $nextAttacker = $teams[intval(!$currentAttackerIndex)];
                break;

            // after these actions both team have chance to do the next attack
            case FinalActions::SCORE1:
            case FinalActions::SHOOT:
            case FinalActions::FAIL:
                $nextAttacker = (mt_rand(0, 90) > mt_rand(0, 100) ?
                    $teams[intval(!$currentAttackerIndex)] :
                    $teams[intval($currentAttackerIndex)]);
                break;

            // FOUL actions randomly could be from both side so both team have chance to do the next attack
            case FinalActions::FOUL:
                $nextAttacker = (mt_rand(0, 100) > mt_rand(0, 70) ?
                    $teams[intval($currentAttackerIndex)] :
                    $teams[!intval($currentAttackerIndex)]);
                break;

            //by default choose other team after current attack
            default:
                $nextAttacker = $teams[mt_rand(0, 1)];
                break;
        }

        // gets next attacker id
        $nextAttacker = $teams[mt_rand(0, 1)];

        return $nextAttacker;
    }

    /**
     * mark game as started in database
     * @return Game
     */
    public function markGameStarted (): Game
    {
        $game = $this->game->refresh();

        $game->started = true;
        $game->save();

        // game started
        $this->addTeamsToStandings();

        return $game;
    }

    /**
     * gets the date of the game
     *
     * @return string
     */
    public function getGameDate (): string
    {
        return date('Y-m-d', strtotime($this->game->game_date));
    }

    /**
     * take a random action for current player
     *
     * @return string
     */
    public function pickRandomAction (): string
    {
        $actions = FinalActions::toArray();

        shuffle($actions);

        return $actions [0];
    }

    /**
     * calculates score for the given action
     *
     * @param $actions
     *
     * @return int
     */
    public function calculateScore (string $actions): int
    {
        switch ($actions) {
            case FinalActions::SCORE1:
                $score = 1;
                break;

            case FinalActions::SCORE2:
                $score = 2;
                break;

            case FinalActions::SCORE3:
                $score = 3;
                break;

            default:
                $score = 0;
                break;
        }

        return $score;
    }

    /**
     * add teams to standing table for further update and calculation
     */
    protected function addTeamsToStandings (): void
    {
        // add home team to standing table
        Standing::firstOrCreate([
            'game_id'   => $this->game->id,
            'team_id'   => $this->game->home_id,
            'game_date' => $this->getGameDate(),
            'home_team' => true,
        ]);

        // add visitor team to standing table
        Standing::firstOrCreate([
            'game_id'   => $this->game->id,
            'team_id'   => $this->game->visitor_id,
            'game_date' => $this->getGameDate(),
            'home_team' => false,
        ]);
    }

    /**
     * gets home team's payer array
     *
     * @return array
     */
    public function getHomeTeamPlayers (): array
    {
        return Player::select('id')
            ->where('team_id', $this->game->home->id)
            ->pluck('id')
            ->toArray();
    }

    /**
     * gets visitor team's payer array
     *
     * @return array
     */
    public function getVisitorTeamPlayers (): array
    {
        return Player::select('id')
            ->where('team_id', $this->game->visitor->id)
            ->pluck('id')
            ->toArray();
    }

    /**
     * gets next random attacker team id
     * @return int attacker id
     */
    public function getRandomAttackerTeam (): int
    {
        return $this->teams[array_rand($this->teams)];
    }
}