<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function players ()
    {
        return $this->hasMany('App\Models\Player', 'team_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function games ()
    {
        return $this->hasMany('App\Models\Game', 'team_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function gamePlayers ()
    {
        return $this->hasMany('App\Models\PlayerScore', 'team_id', 'id');
    }
}
