<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Game
 * @package App\Models
 */
class Game extends Model
{
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function home ()
    {
        return $this->hasOne('App\Models\Team', 'id', 'home_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function visitor ()
    {
        return $this->hasOne('App\Models\Team', 'id', 'visitor_id');
    }

    /**
     * players of home team for current game
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function homePlayers ()
    {
        return $this->hasMany('App\Models\PlayerScore', 'game_id', 'id')
            ->where('home_player', 1);
    }

    /**
     * players of visitor team for current game
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function visitorPlayers ()
    {
        return $this->hasMany('App\Models\PlayerScore', 'game_id', 'id')
            ->where('home_player', 0);
    }

    /**
     * total earned score by home team
     *
     * @return int
     */
    public function getHomeScore ()
    {
        return intval($this->quarter_h_1 + $this->quarter_h_2 + $this->quarter_h_3 + $this->quarter_h_4);
    }

    /**
     * total earned score by visitor team
     *
     * @return int
     */
    public function getVisitorScore ()
    {
        return intval($this->quarter_v_1 + $this->quarter_v_2 + $this->quarter_v_3 + $this->quarter_v_4);
    }
}
