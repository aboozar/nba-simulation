<?php
/**
 * Created by PhpStorm.
 * User: aboozar
 * Date: 11/22/18
 * Time: 6:37 PM
 */

namespace App\Enums;

use MyCLabs\Enum\Enum;

/**
 * Final attack action enum
 */
class FinalActions extends Enum
{
    public const SCORE1  = 'SCORE1'; //penalty
    public const SCORE2  = 'SCORE2';
    public const SCORE3  = 'SCORE3';
    public const SHOOT  = 'SHOOT';
    public const FOUL   = 'FOUL';
    public const FAIL   = 'FAIL';
}