<?php
/**
 * Created by PhpStorm.
 * User: aboozar
 * Date: 11/22/18
 * Time: 6:37 PM
 */

namespace App\Enums;

use MyCLabs\Enum\Enum;

/**
 * ReportType enum
 */
class ReportType extends Enum
{
    public const TOTAL   = 'TOTAL';
    public const YEARLY  = 'YEARLY';
    public const MONTHLY = 'MONTHLY';
    public const WEEKLY  = 'WEEKLY';
    public const DAILY   = 'DAILY';
}