<?php

namespace App\Http\Controllers\Api;

use App\Enums\ReportType;
use App\Models\Game;
use App\Models\Player;
use App\Models\Standing;
use App\Models\Week;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class GameController extends Controller
{

    /**
     * run the simulator bash script
     *
     * this actions uses run.sh for running simulator
     * but for manual run use simulator.sh script
     *
     * @param $week
     *
     * @return array
     */
    public function runSimulator ($week)
    {
        $matches = Game::where('week', $week)->get();

        $filtered = $matches->filter(function ($value, $key) {
            return $value->finished == false;
        });

        $matches = $filtered->pluck('id')->toArray();

        if (count($matches))
        {
            $params = $week . ' ' . implode(' ', $matches);

            $command = app_path() . '/../run.sh ' . $params . ' /dev/null 2>&1 &';

            // execute multiple PHP command at same time for covering PHP as thread
            exec($command);

            return [
                'state' => sprintf('simulation for week %s started...', $week),
                'games' => $matches
            ];
        } else {
            return [
                'status' => sprintf('simulation for week %s has started before!', $week)
            ];
        }
    }

    /**
     * return all week games with related data
     * @param $week
     * @todo improve performance by raw query in production
     *
     * @return Game[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function games ($week)
    {
        $games = Game::with([
            'home'        => function ($query) {
                $query->select('id', 'name', 'nickname', 'abbrev', 'logo');
            },
            'visitor'     => function ($query) {
                $query->select('id', 'name', 'nickname', 'abbrev', 'logo');
            },
            'homePlayers',
            'visitorPlayers',
        ])
        ->where('week', $week)
        ->get();

        return $games;
    }

    /**
     * returns all player ids of requested team
     * null for all team
     *
     * @param int|null $team
     *
     * @return array
     */
    public function players (int $team = null)
    {
        $players = Player::all()
            ->pluck('name', 'id');

        return $players->toArray();
    }

    /**
     * returns all standing table data
     *
     * @param string $type ref: App\Enums\ReportType eg, WEEKLY|DAILY
     * @param int $time    eg, 1|10122018
     *
     * @return mixed
     */
    public function standing (string $type = null, int $time = null)
    {
            $standings = Standing::select(
                'team_id',
                DB::raw('SUM(win) AS wins'),
                DB::raw('SUM(loose) AS looses'),
                DB::raw('SUM(score_earned) AS total_earned'),
                DB::raw('SUM(score_lost) AS total_lost')
            )
            ->groupby('team_id')
            ->orderBy('wins', 'DESC')
            ->orderBy('looses', 'ASC')
            ->orderBy('total_Earned', 'DESC')
            ->orderBy('total_lost', 'ASC');


        switch(strtoupper($type))
        {
            case ReportType::WEEKLY: {
                $week = Week::where('no', $time)->first();

                $data = $standings->where('game_date', '>=', $week->start_date)
                    ->where('game_date', '<=', "{$week->end_date} 23:59:59");
                break;
            }

            case ReportType::DAILY: {
                $data = $standings->where('game_date', '<=', date('Y-m-d', strtotime($type)));
                break;
            }

            default:
                /**
                 * do nothing and show total standing
                 * @todo remove default if not required at all
                 **/
                break;
        }

        $ranking = DB::table('teams')
            ->joinSub($standings, 'stnds', function ($join) {
                $join->on('teams.id', '=', 'stnds.team_id');
            })->get();

        return $ranking;
    }
}
