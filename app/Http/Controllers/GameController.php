<?php

namespace App\Http\Controllers;


use App\Enums\ReportType;
use App\Models\Game;

class GameController extends Controller
{
    /**
     * @param string $type ref: App\Enums\ReportType eg, WEEKLY|DAILY
     * @param int $time    eg, 1|10122018
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function standing (string $type = null, int $time = null)
    {
        return view('standing', [
            'type' => $type ?? ReportType::TOTAL,
            'time' => $time,
        ]);
    }

    /**
     * show week scores page
     * @param $week
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function scores ($week)
    {
        $startedGames = Game::where('week', $week)->where('started', 1)->count();

        return view('scores', [
            'started' => $startedGames,
            'week'    => $week,
        ]);
    }
}
