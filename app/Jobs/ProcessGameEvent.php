<?php

namespace App\Jobs;

use Illuminate\Support\Facades\Log;
use App\Models\Game;
use App\Models\PlayerScore;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

/**
 * This event class is responsible for processing and submitting the game data
 * that simulator created.
 *
 * class updates quarters data for each game and also stores player actions too
 *
 * Class ProcessGameEvent
 * @package App\Jobs
 */
class ProcessGameEvent implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    /**
     * keeps event details
     *
     * @var array
     */
    protected $gameEvent;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct (array $gameEvent)
    {
        $this->gameEvent = $gameEvent;
    }

    /**
     * Submit action scores
     *
     * @return void
     */
    public function handle ()
    {
        $gameEvent = $this->gameEvent;

        Log::debug('process game #event and stores required data', $gameEvent);


        $actionForHome = ($gameEvent['team_id'] == $gameEvent['home_team_id']);

        $game = Game::where('id', $gameEvent['game_id'])->first();


        // if event has score
        if ($gameEvent['score'] > 0) {

            // add player score for event
            PlayerScore::updateOrCreate(
                [
                    'game_id'   => $gameEvent['game_id'],
                    'team_id'   => $gameEvent['team_id'],
                    'player_id' => $gameEvent['player']['player_id'],
                ],
                [
                    'week'                             => $gameEvent['week'],
                    'home_player'                      => $actionForHome,
                    'score'                            => DB::raw("score + {$gameEvent['score']}"),
                    "{$gameEvent['player']['action']}" => DB::raw("{$gameEvent['player']['action']} + 1"),
                ]
            );

            // add assist player score for event (if exists)
            if (isset($gameEvent['assist'])) {
                PlayerScore::updateOrCreate(
                    [
                        'game_id'   => $gameEvent['game_id'],
                        'team_id'   => $gameEvent['team_id'],
                        'player_id' => $gameEvent['assist']['player_id'],
                    ],
                    [
                        'week'        => $gameEvent['week'],
                        'home_player' => $actionForHome,
                        'assist'      => DB::raw("assist + 1"),
                    ]
                );
            }

            if ($actionForHome) {
                $qField = 'quarter_h_' . $gameEvent['quarter'];
            } else {
                $qField = 'quarter_v_' . $gameEvent['quarter'];
            }

            $game->{$qField} = DB::raw("{$qField} + {$gameEvent['score']}");
        }

        // if action has score and it's not a penalty
        if ($gameEvent['score'] != 1) {
            if ($actionForHome) {
                $game->home_attacks = DB::raw('home_attacks + 1');
            } else {
                $game->visitor_attacks = DB::raw('visitor_attacks + 1');
            }
        }

        $game->save();
    }
}
