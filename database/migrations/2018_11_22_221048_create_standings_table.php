<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStandingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('standings', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('team_id')
                ->comment('team identifier');

            $table->unsignedInteger('game_id')
                ->comment('game id');

            $table->boolean('home_team')
                ->comment('home team_indicator');

            $table->date('game_date')
                ->comment('the date of earned scores / match');

            $table->unsignedSmallInteger('win')->default(0)
                ->comment('number of wins during the week games');

            $table->unsignedSmallInteger('loose')->default(0)
                ->comment('number of looses during the week games');

            $table->unsignedInteger('score_earned')->default(0)
                ->comment('total scores that team earned during the week games');

            $table->unsignedInteger('score_lost')->default(0)
                ->comment('total scores that other competitors earned during the week games');


            $table->foreign('team_id')->references('id')->on('teams'); //foreign key to teams
            $table->foreign('game_id')->references('id')->on('games'); //foreign key to teams

            $table->unique(['game_id', 'team_id']); // unique game and team
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('standings');
    }
}
