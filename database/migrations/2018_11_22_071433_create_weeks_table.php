<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWeeksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {
        // this table is just created for keeping simulations status
        Schema::create('weeks', function (Blueprint $table) {
            $table->increments('id');

            $table->string('season', 16);
            $table->unsignedSmallInteger('no');
            $table->date('start_date');
            $table->date('end_date');

            $table->boolean('running')->default(false);
            $table->boolean('finished')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down ()
    {
        Schema::dropIfExists('weeks');
    }
}
