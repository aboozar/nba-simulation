<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayerScoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('player_scores', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('player_id')->comment('player identifier');
            $table->unsignedInteger('team_id')->comment('team identifier');
            $table->unsignedInteger('game_id')->comment('game identifier');
            $table->unsignedSmallInteger('week')->nullable()->comment('week identifier'); // <-- against db normalization for performance
            $table->boolean('home_player')->comment('team indicator');

            $table->unsignedSmallInteger('score1')->default(0)->comment('number of score1|penalty scores');
            $table->unsignedSmallInteger('score2')->default(0)->comment('number of 2 points scores');
            $table->unsignedSmallInteger('score3')->default(0)->comment('number of 3 points scores');
            $table->unsignedSmallInteger('assist')->default(0)->comment('number of assists that players did in match');;

            $table->unsignedSmallInteger('score')->default(0)->comment('total score that player earned in one game');

            $table->foreign('team_id')->references('id')->on('teams'); //foreign key to teams
            $table->foreign('game_id')->references('id')->on('games'); //foreign key to matches
            $table->foreign('player_id')->references('id')->on('players'); //foreign key to players

            $table->unique(['game_id', 'team_id', 'player_id']); // unique player/game
            $table->index('player_id'); // fetch performance improvement
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('player_scores');
    }
}
