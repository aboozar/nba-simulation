<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teams', function (Blueprint $table) {
            $table->unsignedInteger('id')->comment('team id from API');

            $table->string('name', 32)->comment('team name. eg: Boston Celtics');
            $table->string('nickname', 32)->comment('team name. eg: team_name');
            $table->string('code', 16)->unique()->comment('team code. eg: hawks for Atlanta Hawks');
            $table->string('abbrev', 3)->unique()->comment('team name abbreviation. eg: ATL for Atlanta Hawks');

            $table->string('city', 32)->comment('team city'); // should be in separate table
            $table->string('state', 2)->comment('team state'); // should be in separate table

            $table->string('logo', 255)->comment('team logo');

            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teams');
    }
}
