<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Enums\FinalActions;
class CreateGameEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('game_events', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('game_id')->comment('game identifier');
            $table->unsignedInteger('team_id')->comment('team identifier'); // against db normalization for performance
            $table->unsignedInteger('player_id')->comment('player identifier');

            $table->enum('action', FinalActions::toArray())->comment('final action that occurred');
            $table->unsignedSmallInteger('score')->default(0)->comment('scores earned');
            $table->string('message')->comment('scores earned');

            $table->unsignedSmallInteger('quarter')->comment('event quarter');

            $table->foreign('team_id')->references('id')->on('teams'); //foreign key to teams
            $table->foreign('game_id')->references('id')->on('games'); //foreign key to games
            $table->foreign('player_id')->references('id')->on('players'); //foreign key to players
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('game_events');
    }
}
