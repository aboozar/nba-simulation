<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
            $table->unsignedInteger('id')->unique();

            $table->unsignedInteger('home_id')->comment('host team');
            $table->unsignedInteger('visitor_id')->comment('visitor team');

            $table->dateTime('game_date')->comment('exact game start date and time');

            $table->unsignedSmallInteger('week')->comment('week number of a NBA season');
            $table->unsignedSmallInteger('day')->comment('day number of the week');

            $table->unsignedSmallInteger('quarter_h_1')->default(0)->comment('quarter 1 score for home');
            $table->unsignedSmallInteger('quarter_h_2')->default(0)->comment('quarter 2 score for home');
            $table->unsignedSmallInteger('quarter_h_3')->default(0)->comment('quarter 3 score for home');
            $table->unsignedSmallInteger('quarter_h_4')->default(0)->comment('quarter 4 score for home');

            $table->unsignedSmallInteger('quarter_v_1')->default(0)->comment('quarter 1 score for visitor');
            $table->unsignedSmallInteger('quarter_v_2')->default(0)->comment('quarter 2 score for visitor');
            $table->unsignedSmallInteger('quarter_v_3')->default(0)->comment('quarter 3 score for visitor');
            $table->unsignedSmallInteger('quarter_v_4')->default(0)->comment('quarter 4 score for visitor');

            $table->unsignedInteger('winner_id')->nullable()->comment('winner team id');

            $table->unsignedInteger('home_attacks')->default(0)->comment('attack counts of home team');
            $table->unsignedInteger('visitor_attacks')->default(0)->comment('attack counts of visitor team');

            $table->boolean('started')->default(false)->comment('flag to detect if game started or not');
            $table->boolean('finished')->default(false)->comment('flag to detect if game finished or not');
            $table->boolean('processed')->default(false)->comment('flag to detect if game calculation processed or not');

            $table->foreign('home_id')->references('id')->on('teams'); //foreign key to teams
            $table->foreign('visitor_id')->references('id')->on('teams'); //foreign key to teams

            $table->primary('id'); // because id is not incremental and comes from API
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('games');
    }
}
