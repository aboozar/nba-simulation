<?php

use Illuminate\Database\Seeder;
use JasonRoman\NbaApi\Client\Client;
use JasonRoman\NbaApi\Request\Data\Cms\Schedule\ScheduleNbaGamesRequest;
use App\Models\Game;
use App\Models\Team;
use App\Models\Week;

class GamesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * @throws Exception
     * @return void
     */
    public function run ()
    {
        $teams = Team::all()->pluck('id', 'abbrev');

        $client = new Client();

        $request = ScheduleNbaGamesRequest::fromArray([
            'year' => 2018,
        ]);

        $response = $client->request($request);

        $gamesInfo = $response->getResponseBody();

        // fixing the json problem!
        $matchesInfo = str_replace("0721800001", "721800001", $gamesInfo);

        $matchesInfo = json_decode($matchesInfo);


        $i = 0;
        foreach ($matchesInfo->sports_content->schedule->game as $match) {
            $gameExactTime = date('Y/m/d', strtotime($match->dt));

            // season 2018-2019 start date (https://hashtagbasketball.com/advanced-nba-schedule-grid)
            $seasonStart = '2018/10/15';

            // season 2018-2019 end date  (https://hashtagbasketball.com/advanced-nba-schedule-grid)
            $seasonEnd = '2019/04/15';

            try {
                // Filter only current NBA season games
                if (strtotime($gameExactTime) >= strtotime($seasonStart) && strtotime($gameExactTime) < strtotime($seasonEnd)) {

                    $week = $this->dateDiffInDays($seasonStart, $gameExactTime, 7) + 1;
                    $day  = ($this->dateDiffInDays($seasonStart, $gameExactTime) % 7) + 1;

                    $game = new Game();

                    $game->id         = $match->id;
                    $game->home_id    = $teams[$match->h_abrv];
                    $game->visitor_id = $teams[$match->v_abrv];
                    $game->game_date  = $match->dt;
                    $game->week       = $week;
                    $game->day        = $day;

                    $game->save();
                    $i++;
                }
            } catch (\Exception $e) {
                //Log::debug('not--important--error: ' . $e->getMessage()); // just for sure
                continue;
            }
        }

        for ($i = 1; $i <= $week; $i++) {
            $w             = new Week();
            $w->no         = $i;
            $w->season     = '2018-2019'; // hard coded just for testing purposes
            $w->start_date = date('y/m/d', strtotime(strval(($i - 1) * 7) . ' days', strtotime($seasonStart)));
            $w->end_date   = date('y/m/d', strtotime(strval($i * 7 - 1) . ' days', strtotime($seasonStart)));
            $w->save();
        }


        $this->command->info(sprintf('Statistics: games for %d weeks populated', $week - 1));
        $this->command->info(sprintf('Statistics: %d matches injected from API', $i));
    }

    public function dateDiffInDays ($date1, $date2, $diffDays = 1)
    {
        $first  = DateTime::createFromFormat('Y/m/d', $date1);
        $second = DateTime::createFromFormat('Y/m/d', $date2);

        return floor($first->diff($second)->days / $diffDays);
    }
}
