<?php

use Illuminate\Database\Seeder;
use App\Models\Team;
use App\Models\Player;

class PlayersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run ()
    {
        $teams = Team::all();

        $faker = Faker\Factory::create();

        foreach ($teams as $team) {
            for ($i = 1; $i <= 10; $i++) {
                $player          = new Player();
                $player->team_id = $team->id;
                $player->name    = $faker->firstName . ' ' . $faker->lastName;
                $player->save();
            }
        };
    }
}
