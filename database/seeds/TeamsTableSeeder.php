<?php

use Illuminate\Database\Seeder;
use JasonRoman\NbaApi\Client\Client;
use JasonRoman\NbaApi\Request\Data\Cms\Teams\SportsMetaTeamsRequest;
use App\Models\Team;

class TeamsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run ()
    {
        $client = new Client();

        $request  = SportsMetaTeamsRequest::fromArray();
        $response = $client->request($request);

        $sportsInfo = $response->getFromJson();

        $teams = $sportsInfo->sports_content->teams->team;

        $i = 0;
        foreach ($teams as $team) {
            $nbaTeam = new Team();
            if ($team->is_nba_team) {
                $nbaTeam->id       = $team->team_id;
                $nbaTeam->name     = $team->team_name;
                $nbaTeam->nickname = $team->team_nickname;
                $nbaTeam->code     = $team->team_code;
                $nbaTeam->abbrev   = $team->team_abbrev;
                $nbaTeam->city     = $team->city;
                $nbaTeam->state    = $team->state;
                $nbaTeam->logo     = sprintf('https://stats.nba.com/media/img/teams/logos/%s_logo.svg', strtoupper($nbaTeam->abbrev));
                $nbaTeam->save();
                $i++;
            }
        }

        $this->command->info(sprintf('Statistics: %d team(s) injected from API', $i));
    }
}
