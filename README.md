# NBA simulation task

## Requirements

- Unix/Linux based OS for running simulator bash script
- Mysql/Postgres database 
- PHP >= 7.1.3
- php-json extension
- php-mysql extension
- php-pgsql
- nodejs >= 8.0
- Redis server
- php-redis
- Laravel echo server or Pusher.com account
- supervisor / supervisord for keeping workers alive
- internet connection (for db seeder that used NBA API for data population) and maybe Pusher.com access


## Extra information
Project's database seeders using [hashtagbasketball.com](https://hashtagbasketball.com/) APIs for data population
 

### Database setup
1. create a database (and username) in your mysql server

```bash
eg,

CREATE DATABASE nba DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_unicode_ci; 
GRANT ALL PRIVILEGES ON nba.* TO 'nba_user'@'%' IDENTIFIED BY 'nba_pass' WITH GRANT OPTION;
FLUSH PRIVILEGES;

```

## Installation

#### clone the repository 

```bash
git clone https://github.com/iamtartan/nba-simulation-task
``` 

#### copy ```.env-example``` to ```.env``` from project's root path

```bash
cp .env-sample .env
```
#### update your DB config in ```.env``` file

```ini
eg,

DB_DATABASE=nba
DB_USERNAME=nba_user
DB_PASSWORD=nba_pass

# if you want use pusher (not recommended because of free account limitations"
PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=
PUSHER_APP_CLUSTER=eu

```

#### fix ```storage``` and ```bootstrap/cache``` folders permissions (if required)
  
```bash
chown www-data:www-data -R storage bootstrap/cache
# or for development
chmod 777 -R storage bootstrap/cache #(just for dev environment)

```

#### run npm install js packages (on projects root path)

```bash
npm install
npm run production
```

#### Install Laravel Echo Server (our Websocekt server for NBA simulator project project):
You can find about Laravel Echo server here: https://github.com/tlaverdure/laravel-echo-server

Note: Check the requirements at the top!

Run the following (as stated in the document)

```bash
sudo npm install -g laravel-echo-server
```
And then run the init in order to get your laravel-echo-server.json file generated in the app root (which we will need to configure).

```bash
laravel-echo-server init
```

Finally start socket server
 
```bash
laravel-echo-server start 
```

You can keep websocket running by supervisor-d:

Inside your ```/etc/supervisor/conf.d/laravel-echo.conf ```(just create this file inside your conf.d folder) place the following:

```ini
[program:laravel-echo]
directory=/var/www/aboozar/nba-simulator/
process_name=%(program_name)s_%(process_num)02d
command=laravel-echo-server start
autostart=true
autorestart=true
user=your-linux-user
numprocs=1
redirect_stderr=true
stdout_logfile=/var/www/aboozar/nba-simulator/storage/logs/echo.log
```

#### run php composer (on projects root path)

```bash
composer install
```

#### generate a new laravel hash key

```bash
php artisan key generate
```

#### run DB migrations and data populations

```bash
php artisan migrate --seed
```

#### run projects via your own webserver or built in PHP webserver

```bash
php artisan serve
```

#### open project's homepage by given URL from previous step in your browser

```plain
eg, Laravel development server started: <http://127.0.0.1:8000>
```

#### Queue workers

```ini
[program:laravel-worker]
process_name=%(program_name)s_%(process_num)02d
command=php /var/www/aboozar/nba-simulator/artisan queue:work --queue=quarter1,quarter2,quarter3,quarter4
autostart=true
autorestart=true
user=aboozar
numprocs=8
redirect_stderr=true
stdout_logfile=/var/www/aboozar/nba-simulator/storage/logs/q-workers.log
```

### Reset all data

use following command for resetting all DB data and populating tables by fresh data

```bash
php artisan migrate:fresh --seed
```

### Running simulator manually

```bash
# from projects root for only one matches
php artisan game:start $MATCH_ID

```