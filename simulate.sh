#!/bin/bash

DIR=$(pwd)

# first parameter is week number

#php "$DIR/"artisan push:week "$1" > /dev/null 2>&1 &

#shift

# rest parameters are game id

while [ "$1" != "" ]; do

    echo "start game $1"

    php "$DIR/"artisan game:simulate "$1" > /dev/null 2>&1 &
    sleep .1
    shift

done

