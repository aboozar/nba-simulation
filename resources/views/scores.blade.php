@extends('layouts.app')

@section('content')
<div class="content">
    <div class="row">
        <div class="col-12">
            <scoreboard :initial-week="{{$week}}" :init-started="{{$started}}"></scoreboard>
        </div>
    </div>
</div>
@endsection