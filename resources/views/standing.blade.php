@extends('layouts.app')

@section('content')
<div class="content">
    <div class="row">
        <div class="col-12">
            <standing-table initial-type="{{$type}}" initial-time="{{$time}}"></standing-table>
        </div>
    </div>
</div>
@endsection