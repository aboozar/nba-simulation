@extends('layouts.app')

@section('content')
    <div class="text-center">
        <a href="{{route('scores::view', ['week' => 1])}}">
            Click to start
        </a>
    </div>
    <a href="{{route('scores::view', ['week' => 1])}}">
        <img src="{{asset('img/intro.jpg')}}" class="img-fluid" />
    </a>
@endsection

@push('js')
<script type="text/javascript" charset="UTF-8">
    //
</script>
@endpush